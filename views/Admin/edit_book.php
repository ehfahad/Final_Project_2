<?php

include_once('../../vendor/autoload.php');
use App\Library\Library;
use App\Message\Message;
use App\Utility\Utility;

$book= new Library();
$book->prepare($_GET);
$singleItem=$book->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Book</h2>
    <form role="form" method="post" action="update_book.php" enctype="multipart/form-data">
        <div class="form-group">


            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <label>Book Name:</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Book Name" value="<?php echo $singleItem->name?>">
            <label>Author Name:</label>
            <input type="text" name="author" class="form-control" id="author" placeholder="Enter Author Name" value="<?php echo $singleItem->author?>">
            <label>Edition:</label>
            <input type="text" name="edition" class="form-control" id="edition" placeholder="Enter Edition" value="<?php echo $singleItem->edition?>">
            <label>Cover</label>
            <input type="file" name="cover" class="form-control" >
            <img src="../../Resources/Images/<?php echo $singleItem->cover?>"alt="cover" height="421px" width="270px" class="img-responsive">

            <div class="form-group">
                <label class="col-md-2 control-label">Select Location </label>

                <div class="col-md-4">
                    <select class="form-control" name="location" id="sel1">
                        <option>Select any</option>
                        <option>Shelf 1</option>
                        <option>Shelf 2</option>
                        <option>Shelf 3</option>
                        <option>Shelf 4</option>
                    </select>
                </div>


                <label class="col-md-2 control-label">Select Category </label>
                <div class="col-md-4">
                    <select class="form-control" name="category" id="sel1">
                        <option>Select any</option>
                        <option>Science Fiction</option>
                        <option>Novel</option>
                        <option>Physics</option>
                        <option>Programming</option>
                        <option>Reference Book</option>
                    </select>
                </div>

            <label>amount</label>
            <input type="number" name="amount" class="form-control" id="amount" placeholder="Enter Number" value="<?php echo $singleItem->amount?>">
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
            <a href="index.php" class="btn btn-warning">Cancel</a>
    </form>
</div>

</body>
</html>

