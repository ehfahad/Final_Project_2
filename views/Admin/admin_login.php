<?php

session_start();

include_once('../../vendor/autoload.php');
use App\Message\Message;
use App\Utility\Utility;

?>


<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
    <title>Login and Registration Form of Library Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../../Resources/Login/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="../../Resources/Login/css/style.css" />
    <link rel="stylesheet" type="text/css" href="../../Resources/Login/css/animate-custom.css" />
    <link rel="stylesheet" type="text/css" href="../../Resources/bootstrap/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="../../Resources/bootstrap/css/bootstrap.min.css" />
</head>
<body>
<div class="container">
    <!-- Codrops top bar -->
    <div class="codrops-top">

                <span class="right">
                    <a href=" http://tympanus.net/codrops/2012/03/27/login-and-registration-form-with-html5-and-css3/">

                    </a>
                </span>
        <div class="clr"></div>
    </div><!--/ Codrops top bar -->
    <header>
        <h1><span class="label-danger">Permission Restricted !!! Only for<strong> Admin</strong></span></h1>
        <nav class="codrops-demos">



        </nav>
        <?php if(array_key_exists("message",$_SESSION) && !empty($_SESSION['message'])): ?>
            <div id="message" class="alert alert-info">
                <center> <?php echo Message::message() ?></center>
            </div>
        <?php endif; ?>
    </header>
    <section>
        <div id="container_demo" >
            <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>
            <div id="wrapper">
                <div id="login" class="animate form">
                    <form action="../Authentication/admin_login_approve.php" method="post" autocomplete="on">
                        <h1>Log in</h1>
                        <p>
                            <label for="username" class="uname" data-icon="u" > Username</label>
                            <input id="username" name="username" required="required" type="text" placeholder="Enter Username"/>
                        </p>
                        <p>
                            <label for="password" class="youpasswd" data-icon="p"> Password </label>
                            <input id="password" name="password" required="required" type="password" placeholder="Enter Password" />
                        </p>

                        <p class="login button">
                            <input type="submit" value="Login" />
                        </p>
                    </form>
                </div>


            </div>
        </div>
    </section>
</div>
</body>
</html>