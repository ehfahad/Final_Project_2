<?php

session_start();

include_once('../../vendor/autoload.php');
use App\Library\Library;
use App\Utility\Utility;
use App\Message\Message;

$obj = new Library();
$due_date = $obj->prepare($_POST)->returnBook();
Utility::d($due_date);

$date1=date_create(date("Y-m-d"));
$date2=date_create($due_date);
$diff=date_diff($date1,$date2);
$delay = $diff->format("%R%a");

if($delay<1)
    Message::message("Fine amount is 0.00 TK");
else{
    $amount = (5.00*$delay);
    Message::message("Fine amount is ".$amount." TK");
}
Utility::redirect('return_book.php');