<?php
//var_dump($_GET);
include_once('../../vendor/autoload.php');
use App\Library\Library;
use App\Message\Message;
use App\Utility\Utility;

$book = new \App\Library\Library;
$book->prepare($_GET);
$singleItem = $book->view();
//Utility::d($singleItem);
?>


<!DOCTYPE html>
<html>
<head>
    <title>Library Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="../../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../Resources/bootstrap/css/styles.css" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">


    <link href="../../Resources/bootstrap/css/forms.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
</head>
<body>


<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="index.php">Library Management System</a></h1>
                </div>
            </div>
            <form action="">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group form">
                                <input type="text" class="form-control" placeholder="Search...">
	                       <span class="input-group-btn">
	                         <button class="btn btn-primary" type="button">Search</button>
	                       </span>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="admin_logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>


                    <li><a href="forms.php"><i class="glyphicon glyphicon-tasks"></i>Add Book</a></li>
                    <li><a href="requested_books.php"><i class="glyphicon glyphicon-book"></i>Requested Book</a></li>
                    <li><a href="return_book.php"><i class="glyphicon glyphicon-backward"></i>Return Book</a></li>

                    <li class="submenu">
                        <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="../../index.php">Normal Index</a></li>
                            <li><a href="../index.php">User Index</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>


        <div class="col-md-10">


            <div class="row">
                <div class="col-md-12">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Admin Panel</div>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">ID: <?php echo $singleItem->id ?></li>
                                <li class="list-group-item">Book Name: <?php echo $singleItem->name ?></li>
                                <li class="list-group-item">Author Name: <?php echo $singleItem->author ?></li>
                                <li class="list-group-item">Edition: <?php echo $singleItem->edition ?></li>
                                <li class="list-group-item">Category: <?php echo $singleItem->category ?></li>
                                <li class="list-group-item">Location: <?php echo $singleItem->location ?></li>
                                <label>Book Cover:</label>
                                <img src="../../Resources/Images/<?php echo $singleItem->cover ?>" alt="cover"
                                     height="421px" width="270px" class="img-responsive">
                            </ul>
                            <a href="index.php" role="button" class="btn btn-primary">Done</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  Page content -->
    </div>
</div>

<footer>
    <div class="container">

        <div class="copy text-center">
            Copyright 2014 <a href='#'>Website</a>
        </div>

    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="vendors/form-helpers/js/bootstrap-formhelpers.min.js"></script>

<script src="vendors/select/bootstrap-select.min.js"></script>

<script src="vendors/tags/js/bootstrap-tags.min.js"></script>

<script src="vendors/mask/jquery.maskedinput.min.js"></script>

<script src="vendors/moment/moment.min.js"></script>

<script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

<!-- bootstrap-datetimepicker -->
<link href="vendors/bootstrap-datetimepicker/datetimepicker.css" rel="stylesheet">
<script src="vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>


<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
      rel="stylesheet"/>
<script
    src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../../Resources/bootstrap/js/custom.js"></script>
</body>
</html>