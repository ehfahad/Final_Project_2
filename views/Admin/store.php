<?php
include_once('../../vendor/autoload.php');
use App\Library\Library;


if((isset($_FILES['cover'])&& !empty($_FILES['cover']['name']))){
    $image_name= time().$_FILES['cover']['name'];
    $temporary_location= $_FILES['cover']['tmp_name'];

    move_uploaded_file( $temporary_location,'../../Resources/Images/'.$image_name);
    $_POST['cover']=$image_name;

}

$profile_picture= new Library();
$profile_picture->prepare($_POST)->store();



