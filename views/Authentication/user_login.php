<?php
session_start();
include_once('../../vendor/autoload.php');

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_registered();

if($status){
    $_SESSION['user_id']=$_POST['id'];
    //Utility::dd($_SESSION);
    return Utility::redirect('../../views/index.php');
}
else{
    Message::message("Wrong username or password");
    return Utility::redirect("../user_login_signup.php");

}
