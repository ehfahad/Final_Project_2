<?php
session_start();
include_once('../../vendor/autoload.php');

use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_registered();

if($status){
    $_SESSION['admin_name']=$_POST['username'];
    //Utility::dd($_SESSION);
    return Utility::redirect('../Admin/index.php');
}
else{
    Message::message("Wrong username or password");
    return Utility::redirect("../Admin/admin_login.php");
}
