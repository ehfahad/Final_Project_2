<?php

session_start();

include_once('vendor/autoload.php');

use App\Utility\Utility;
use App\Library\Library;
use App\Message\Message;

if (array_key_exists("itemPerPage", $_SESSION)) {
    if (array_key_exists("itemPerPage", $_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
} else
    $_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$obj = new Library();
$totalItem = $obj->count();
//echo $totalItem;
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
    $pageNumber = $_GET['pageNumber'];
else
    $pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
    $class = ($pageNumber == $count) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
    $allInfo = $obj->paginator($pageStartFrom, $itemPerPage);
}
if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && isset($_GET['category']))  {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}
if ((strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) && isset($_GET['search'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}

//$availableTitle=$obj->getAllTitle();
//$comma_separated= '"'.implode('","',$availableTitle).'"';
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>

<!DOCTYPE html>
<html>
<head>
    <style>
        .leftpad5 {
            margin-top: -19px;
            margin-left: -23px;
        }


    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="Resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="Resources/bootstrap/css/style3.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="https://code.jquery.com/jquery.js"></script>


</head>
<body>

<div class="container">


    <nav class="row navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a class="active" href="index.php" >Home</a></li>
                    <li><a href="index.php?category=<?php echo "Reference Book" ?>" >Reference Book</a></li>
                    <li><a href="index.php?category=<?php echo "Science Fiction" ?>" >Science Fiction</a></li>
                    <li><a href="index.php?category=<?php echo "Novel" ?>" >Novel</a></li>
                    <li><a href="index.php?category=<?php echo "Physics" ?>" >Physics</a></li>
                    <li><a href="index.php?category=<?php echo "Programming" ?>" >Programming</a></li>
                </ul>


                <div class="nav rightmaring navbar-nav navbar-right">
                    <li class="toppad5">
                        <form class="form-inline" action="index.php" method="get">
                            <input  class="form-control" placeholder="Search..." type="text" name="search">
                            <button class="btn btn-info" type="submit">Search</button>
                        </form>
                    </li>
                    <div class="col-sm-4 ">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle rightmaring" data-toggle="dropdown">LogIn/SignUp <b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="views/user_login_signup.php">Login as Student</a></li>
                                    <li><a href="views/Admin/admin_login.php">Login as Admin</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>


                </div>


            </div>
        </div>
    </nav>


    <div class="row slidemargin">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="Resources/Images/slide_image_1.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>পলান সরকার</h3>
                        <p>বাংলাদেশের বাতিঘর বিস্তীর্ন জনপদ আলোকিত করার দায়িত্ব নিয়েছেন তিনি একাই</p>
                    </div>
                </div>

                <div class="item">
                    <img src="Resources/Images/slide_image_2.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>হুমায়ূন আজাদ</h3>
                        <p>একজন প্রথাবিরোধী লেখক . মৌলবাদী শক্তির বিরুদ্ধে তিনি ছিলেন এক লড়াকু যোদ্ধা</p>
                    </div>
                </div>

                <div class="item">

                    <img src="Resources/Images/slide_image_3.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>হুমায়ূন আহমেদ</h3>
                        <p>বাংলা সাহিত্যের অন্যতম জনপ্রিয় লেখক। হিমু, মিসির আলীর মত চরিত্র সৃষ্টি করেছেন তিনি</p>
                    </div>
                </div>

                <div class="item">
                    <img src="Resources/Images/slide_image_4.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>Ahmed Chafa</h3>
                        <p>Most prominent writer.</p>
                    </div>
                </div>

                <div class="item">
                    <img src="Resources/Images/slide_image_4.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>Ahmed Chafa</h3>
                        <p>Most prominent writer.</p>
                    </div>
                </div>

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>


    <div class="row background">
        <h1 class="text-center">সর্বাধিক পঠিত</h1>
        <p class="text-center">যে কোন কেউ এই লাইব্রেরি থেকে বই নিয়ে আলোকিত হতে পারেন</p>

        <div class="col-sm-4 ">

            <div class="imgWrap">
                <img src="Resources/Images/page1_pic7.jpg" alt="polaroid"/>
                <div class="imgDescription text-center">
                    <h3>হাসুলী বাকের উপকথা </h3>
                    <h4>জসীম উদ্দিন</h4>
                    <h5>Shrot info about book</h5>

                </div>

            </div>

        </div>

        <div class="col-sm-4 ">

            <div class="imgWrap">
                <img src="Resources/Images/page1_pic7.jpg" alt="polaroid"/>
                <div class="imgDescription text-center">
                    <h3>হাসুলী বাকের উপকথা </h3>
                    <h4>জসীম উদ্দিন</h4>
                    <h5>Shrot info about book</h5>
                </div>
            </div>
        </div>
    </div>
</div>


</body>


<!--

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Book Name</th>
                    <th>Author Name</th>
                    <th>Edition</th>
                    <th>Book Cover</th>
                    <th>Location</th>
                    <th>Availablity</th>

                </tr>
                </thead>
                <tbody>
                <?php $sl = 0;
foreach ($allInfo as $info) {
    $sl++; ?>

                <tr>
                    <td width="5%"> <?php echo $sl ?> </td>
                    <td width="5%"> <?php echo $info->name ?></td>
                    <td width="15%"> <?php echo $info->author ?> </td>
                    <td class="text-justify" width="10%"><?php echo $info->edition ?></td>
                    <td class="text-justify" width="20%"><img src="Resources/images/page1_pic5.jpg" class="img-responsive" width="100px" height="100px"/></td>
                    <td class="text-justify" width="10%"><?php echo $info->location ?></td>
                    <?php if ($info->amount > 0) : ?>
                        <td width="15%"><a href="" class="btn btn-success leftpad5" role="button">Available<span class="badge"> <?php echo $info->amount ?> </span></a>
                        <?php endif ?>
                            <?php if ($info->amount == 0) : ?>
                    <td width="15%"><a href=""  class="btn btn-danger leftpad5" role="button">Not Available<span class="badge"> <?php echo $info->amount ?> </span></a>
                        <?php endif ?>
                        </tr>

                <?php } ?>


                </tbody>
            </table>
            --!>
        </div>
            <?php if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) { ?>
                <ul class="pagination">
                    <?php if ($pageNumber > 1): ?> <li><a href='index.php?pageNumber=<?php echo $pageNumber - 1 ?>'> Prev </a></li> <?php endif; ?>
                    <?php echo $pagination; ?>
                    <?php if ($pageNumber < $totalPage): ?> <li><a href='index.php?pageNumber=<?php echo $pageNumber + 1 ?>'> Next </a></li> <?php endif; ?>
                </ul>
            <?php } ?>

    </div>
    <script>
        $('#message').show().delay(2000).fadeOut();


//        $(document).ready(function(){
//            $("#delete").click(function(){
//                if (!confirm("Do you want to delete")){
//                    return false;
//                }
//            });
//        });
        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

    </script>
        <script>
            $( function() {
                var availableTags = [
                    <?php echo $comma_separated ?>
                ];
                $( "#title" ).autocomplete({
                    source: availableTags
                });
            } );
        </script>

    </body>
    </html>
